<?php

$user = $_GET["user"];

switch ($user) {
    case "formulaire":
        include("vues/formClient.php");
        break;
    case "verif":
        if (isset($_SESSION["autoClient"]) and $_SESSION["autoClient"] == "Déjà connecté") {
            $lesProduits = Produit::afficherTous();
            include("vues/listeProduit.php");
        } else {
            $mdp = hash('sha256', $mdp);
            $rep = Client::verifier($mail, $mdp);

            if ($rep == true) {
                $_SESSION["autoClient"] = "Déjà connecté";
                $infoClient = Client::infoClient($mail, $mdp);
                var_dump($infoClient);
                $_SESSION["clientPrenom"] = $infoClient->prenom;
                $_SESSION["clientNom"] = $infoClient->nom;

                $lesProduits = Produit::afficherTous();
                include("vues/listeProduit.php");
            } else {
                $_SESSION["autoClient"] = '<center><H2 style="color:red;">mot de passe ou mail incorrecte</H2></center>';
                include("vues/formClient.php");
            }
        }
        break;
    case "formulaireNouveau":
        include("vues/formNouveauClient.php");
        break;
    case "verifNouveau":
        $nouveauClient = new Client;
        $mdpNouveau = hash('sha256', $mdpNouveau);
        $nouveauClient->setNom($nomNouveau);
        $nouveauClient->setPrenom($prenomNouveau);
        $nouveauClient->setMail($mailNouveau);
        $nouveauClient->setMdp($mdpNouveau);
        Client::verifierNouveau($nouveauClient);
        include("vues/formClient.php");
        break;
    case "deco":
        unset($_SESSION["autoClient"]);
        unset($_SESSION["client"]);
        include("vues/accueil.php");
        break;
}
