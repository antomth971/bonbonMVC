<?php

$choix = $_GET["choix"];
switch ($choix) {

    case "formulaire":
        include("vues/formAdmin.php");
        break;
    case "verif":
        $lesProduits = Produit::afficherTous();
        if (isset($_SESSION["autorisation"]) and $_SESSION["autorisation"] == "ok") {
            include("vues/accueilAdmin.php");
        } else {
            $mdp = hash('sha256', $mdp);
            $rep = Admin::verifier($login, $mdp);
            if ($rep == true) {
                $_SESSION["autorisation"] = "ok";
                $_SESSION["message"] = "vous êtes bien connecté";
                include("vues/accueilAdmin.php");
            } else {
                include("vues/echecConnexion.php");
            }
        }
        break;
    case "ajout":
        include("vues/formAjout.php");
        break;
    case "confAjout":
        if (isset($_SESSION["autorisation"]) and $_SESSION["autorisation"] == "ok") {
            $nouveauProduit = new Produit;
            $nouveauProduit->setNom($nom);
            $nouveauProduit->setIdCat($idCateg);
            $nouveauProduit->setPrix($prix);
            $nouveauProduit->setPhoto(basename($_FILES["img"]["name"]));
            $nom_image = basename($_FILES["img"]["name"]);
            $chemin = "Images/" . $nom_image;
            move_uploaded_file($_FILES["img"]["tmp_name"], $chemin);
            $nb = Produit::ajouter($nouveauProduit);
            if ($nb == 1) {
                $_SESSION["message"] = "le produit a bien été ajouté";
                $lesProduits = Produit::afficherTous();
                include("vues/accueilAdmin.php");
            }
        }
        break;
    case "modifier":
        $produit = Produit::trouverUnBonbon($modif);
        include("vues/formModifier.php");
        break;
    case "deconnexion":
        Admin::deconnexion();
        include("vues/accueil.php");
        break;
    case "confModifier":
        $idCache = $_POST["idCache"];
        $produitModifier = new Produit;
        $produitModifier->setId($idCache);
        $produitModifier->setNom($nom);
        $produitModifier->setPrix($prix);
        $produitModifier->setIdCat($idCat);
        $produitModifier->setPhoto(basename($_FILES["img"]["name"]));
        $nom_image = basename($_FILES["img"]["name"]);
        $chemin = "Images/" . $nom_image;
        move_uploaded_file($_FILES["img"]["tmp_name"], $chemin);
        Produit::modifier($produitModifier);
        $lesProduits = Produit::afficherTous();
        include("vues/accueilAdmin.php");
        break;
    case "supprimer":
        Produit::supprimer($sup);
        $lesProduits = Produit::afficherTous();
        include("vues/accueilAdmin.php");
        break;
}
