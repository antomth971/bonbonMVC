<?php

class Admin
{
    private int $id;
    private string $login;
    private string $mdp;



    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of login
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set the value of login
     *
     * @return  self
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get the value of mdp
     */
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set the value of mdp
     *
     * @return  self
     */
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    public static function verifier($login, $mdp)
    {
        $sql = MonPdo::getInstance()->prepare("select * from admin where login=:login && mdp=:mdp");
        $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'admin');
        $sql->bindValue(":login", $login);
        $sql->bindValue(":mdp", $mdp);
        $sql->execute();
        $result = $sql->fetchAll();
        $nb_lignes = count($result);
        if ($nb_lignes == 0) {
            $rep = false;
        } else {
            $rep = true;
        }
        return $rep;
    }
    public static function deconnexion()
    {
        if (isset($_SESSION["autorisation"])) {
            unset($_SESSION["autorisation"]);
        }
    }
}
