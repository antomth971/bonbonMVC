<?php

class Produit
{
    private int $id;
    private string $nom;
    private float $prix;
    private string $photo;
    private int $idCat;



    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        if ($id > 0) {
            $this->id = $id;
        } else {
            echo "l'id du produit est inferieur ou égale à 0";
        }
    }

    /**
     * Get the value of photo
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set the value of photo
     *
     * @return  self
     */
    public function setPhoto($photo)
    {

        $this->photo = $photo;
    }

    /**
     * Get the value of prix
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @return  self
     */
    public function setPrix($prix)
    {
        if ($prix > 0) {
            $this->prix = $prix;
        } else {
            echo "prix produit invalide";
        }
    }

    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */
    public function setNom($nom)
    {
        if (strlen($nom) > 1) {
            $this->nom = $nom;
        } else {
            echo "le nom du produit est invalide";
        }
    }
    /**
     * Get the value of idCateg
     */
    public function getIdCat()
    {
        return Categorie::trouverUneCategorie($this->idCat);
    }
    /**
     * Get the value of idCat
     */
    public function getIdCat2()
    {
        return $this->idCat;
    }


    /**
     * Set the value of idCateg
     *
     * @return  self    
     */
    public function setIdCat($idCateg)
    {
        if ($idCateg = 1 || $idCateg = 2) {
            $this->idCat = $idCateg;
        } else {
            echo "l'id de la catégorie saisie est incorrecte";
        }
    }

    public static function afficherTous()
    {
        $req = MonPdo::getInstance()->prepare("SELECT * FROM produit");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Produit');
        $req->execute();
        $lesResultats = $req->fetchAll();
        return $lesResultats;
    }

    public static function trouveCateg($id)
    {
        $req = MonPdo::getInstance()->prepare("SELECT * FROM categorie INNER JOIN produit on produit.idCat=categorie.id where categorie.id=:id");
        $req->bindValue(":id", $id);
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'produit');
        $req->execute();
        $lesResultats = $req->fetchAll();
        return $lesResultats;
    }

    public static function recherche($search)
    {

        // $sql = MonPdo::getInstance()->prepare("SELECT * FROM produit WHERE nom =:noms");
        $sql = MonPdo::getInstance()->prepare("SELECT * FROM produit WHERE nom LIKE :search");

        // $sql->bindValue(":noms", $nom);
        $sql->bindValue(":search", "%$search%", PDO::PARAM_STR);
        $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Produit');

        $sql->execute();

        $lesResultats = $sql->fetchAll();
        return $lesResultats;
    }
    public static function ajouter(Produit $produit)
    {
        $req = MonPdo::getInstance()->prepare("INSERT INTO produit(nom,prix,photo,idCat) VALUES (:nom,:prix,:photo,:idCateg)");
        $req->bindValue(":nom", $produit->getNom());
        $req->bindValue(":prix", $produit->getPrix());
        $req->bindValue(":photo", $produit->getPhoto());
        $req->bindValue(":idCateg", $produit->getIdCat2());
        $result = $req->execute();
        return $result;
    }
    public static function trouverUnBonbon(int $id)
    {
        $req = MonPdo::getInstance()->prepare("SELECT * FROM produit WHERE id=:id");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Produit');
        $req->bindValue(":id", $id);
        $req->execute();
        $result = $req->fetch();
        return $result;
    }

    public static function modifier(Produit $produit)
    {
        try {
            $req = MonPdo::getInstance()->prepare("UPDATE produit SET nom=:nom,prix=:prix,photo=:photo,idCat=:idCateg where id=:id");
            $req->bindValue(":nom", $produit->getNom());
            $req->bindValue(":prix", $produit->getPrix());
            $req->bindValue(":photo", $produit->getPhoto());
            $req->bindValue(":idCateg", $produit->getIdCat2());
            $req->bindValue(":id", $produit->getId());
            $result = $req->execute();
            return $result;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function supprimer($id)
    {
        $req = MonPdo::getInstance()->prepare("DELETE FROM produit WHERE id=:id");
        $req->bindValue(":id", $id);
        $result = $req->execute();
        return $result;
    }
    public static function panier($quantite, $id)
    {
        $req = MonPdo::getInstance()->prepare("SELECT id,nom,prix,prix*:quantite as montant FROM produit WHERE id=:id");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'produit');

        $req->bindValue(":quantite", $quantite);
        $req->bindValue(":id", $id);

        $req->execute();
        $resultat = $req->fetch();

        return $resultat;
    }
}
