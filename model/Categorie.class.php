<?php

class Categorie
{
    private int $id;
    private string $libelle;


    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        if ($id > 0) {
            $this->id = $id;
        } else {
            echo "L'id de ta catégorie est incorrecte";
        }
    }

    /**
     * Get the value of libelle
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set the value of libelle
     *
     * @return  self
     */
    public function setLibelle($libelle)
    {
        if (strlen($libelle) > 3) {
            $this->libelle = $libelle;
        } else {
            echo "Le libelle de de cette categorie";
        }
    }
    public static function trouverUneCategorie($id)
    {
        $req = MonPdo::getInstance()->prepare("SELECT * FROM categorie WHERE id=:id");
        $req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'categorie');
        $req->bindValue(":id", $id);
        $req->execute();
        $result = $req->fetch();
        return $result;
    }
}
