<?php

class Client
{
    private int $id;
    private string $nom;
    private string $prenom;
    private string $mail;
    private string $mdp;

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of prenom
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @return  self
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get the value of mail
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set the value of mail
     *
     * @return  self
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get the value of mdp
     */
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set the value of mdp
     *
     * @return  self
     */
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    public static function infoClient($mail, $mdp)
    {
        $sql = MonPdo::getInstance()->prepare("SELECT * from user where mail=:mail && mdp=:mdp");
        $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'client');
        $sql->bindValue(":mail", $mail);
        $sql->bindValue(":mdp", $mdp);
        $sql->execute();
        $result = $sql->fetch(PDO::FETCH_OBJ);
        return $result;
    }

    public static function verifier($mail, $mdp)
    {
        $sql = MonPdo::getInstance()->prepare("SELECT * from user where mail=:mail && mdp=:mdp");
        $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'client');
        $sql->bindValue(":mail", $mail);
        $sql->bindValue(":mdp", $mdp);
        $sql->execute();
        $result = $sql->fetchAll();
        $nb_lignes = count($result);
        if ($nb_lignes == 0) {
            $rep = false;
        } else {
            $rep = true;
        }
        return $rep;
    }

    public static function verifierNouveau(Client $client)
    {
        try {
            $sql = MonPdo::getInstance()->prepare("INSERT INTO user(nom,prenom,mail,mdp) VALUES (:nom,:prenom,:mail,:mdp)");
            $sql->bindValue(":nom", $client->getNom());
            $sql->bindValue(":prenom", $client->getPrenom());
            $sql->bindValue(":mail", $client->getMail());
            $sql->bindValue(":mdp", $client->getMdp());
            $result = $sql->execute();
            return $result;
        } catch (PDOException $e) {
            // echo "Il y a une erreur" . $e->getMessage();

?>
            <center>
                <a href="user.php" class="btn btn-secondary">Vous avez déjà un compte, veuillez aller vous connecter</a>
            </center>
<?php
        }
    }
}
