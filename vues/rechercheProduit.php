<?php ob_start(); ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <H2>Nos Produits</H2>
        </div>
    </div>
    <div class="col">
        <?php
        foreach ($recherches as $produit) {
        ?>

            <div class="card text-center" style="width: 15rem;">
                <img src="Images/<?= $produit->getPhoto() ?>" class="card-img-top">
                <h5 class="card-title"><?= $produit->getNom() ?></h5>
                <p class='card-text'> <?= $produit->getPrix() ?> €</p>
                <a href="ajout_panier.php?ajout=<?= $produit->getId() ?>" class=" btn btn-danger">Ajouter au Panier</a>


            </div>



        <?php

        }


        ?>
    </div>
</div>
<?php $content = ob_get_clean();
require("template.php"); ?>