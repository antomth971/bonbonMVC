<?php
ob_start();
if (!empty($_SESSION['autoClient'])) {
?>
    <div class="alert alert-succes" role="alert" id="alert">
        <?php echo $_SESSION['autoClient'] ?>
    </div>
<?php
    unset($_SESSION['client']);
}
?>

<form action="index.php?uc=client&user=verif" method="POST">
    <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="text" name="mail" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Mot de passe</label>
        <input type="password" name="mdp" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>

    <button type="submit" class="btn btn-primary">Connexion</button>
</form>
<br><br><br><br>
<center>
    <a href="index.php?uc=client&user=formulaireNouveau" class="btn btn-secondary">Si vous n'avez pas encore de compte, veuillez vous en créer un.</a>
</center>
<?php $content = ob_get_clean();
require("template.php"); ?>