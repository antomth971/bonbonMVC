<?php
ob_start();
if (isset($_SESSION['autoClient']) && $_SESSION['autoClient'] == "Déjà connecté") {
?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <H2>Nos Produits</H2>
            </div>
        </div>
        <center>
            <h3>Bonjour <?= $_SESSION["clientNom"] ?> <?= $_SESSION["clientPrenom"] ?></h3>
        </center>
        <div class="col">
            <?php
            if (!empty($_SESSION['succes'])) {
            ?>
                <div class="alert alert-succes" role="alert" id="alert">
                    <?php echo $_SESSION['succes'] ?>
                </div>
            <?php
                unset($_SESSION['succes']);
            }
            if (isset($_SESSION['panier'])) {
            ?>
                <center>
                    <a class="btn btn-secondary" href="index.php?uc=bonbons&action=panier"> Voir le panier <span class="badge bg-primary"> <?php echo array_sum($_SESSION['panier']) ?></span></a>

                    <br>
                </center>
            <?php
            }
            foreach ($lesProduits as $produit) {
            ?>
                <div class="card text-center" style="width: 15rem;">
                    <img src="Images/<?= $produit->getPhoto() ?>" class="card-img-top">
                    <h5 class='card-title'> <?= $produit->getNom() ?></h5>
                    <p class='card-text'> <?= $produit->getPrix() ?> €</p>

                    <a href="index.php?uc=bonbons&action=ajoutPanier&id=<?= $produit->getId() ?>" class=" btn btn-primary">Ajouter au Panier</a>


                </div>



            <?php
            }


            ?>
        </div>
    </div>
<?php
} else {
?>

    <a href="index.php?uc=client&user=formulaire" class="btn btn-primary">Veuillez-vous connecter ou vous créer un compte</a>


<?php
}
$content = ob_get_clean();
require("template.php"); ?>