<?php
ob_start();
if (isset($_SESSION["autorisation"]) and $_SESSION["autorisation"] == "ok") {
    echo $_SESSION["message"];
    echo "<H1>Bonjour Admin</H1>";

?>

    <table class="table">
        <thead>
            <tr>
                <th>Nom produit</th>
                <th>photo</th>
                <th>Prix</th>
                <th>Nom photo</th>
                <th>Modifier</th>
                <th>supprimer</th>

            </tr>
        </thead>
        <?php
        foreach ($lesProduits as $produit) {
        ?>


            <tbody>
                <tr>
                    <td><?php echo $produit->getNom() ?></td>
                    <td><img src="Images/<?= $produit->getPhoto() ?>" height="100rem"></td>
                    <td><?php echo $produit->getPrix() . "€" ?></td>
                    <td><?php echo $produit->getPhoto() ?></td>
                    <td><a href="index.php?uc=admin&choix=modifier&modif=<?= $produit->getId() ?>" class="btn btn-primary">modifier</a></td>
                    <td><a href="index.php?uc=admin&choix=supprimer&sup=<?= $produit->getId() ?>" class="btn btn-danger" onClick='return confirm("Voulez-vous supprimer le bonbon <?php echo $produit->getNom() ?>?")'>Supprimer</a></td>

                </tr>




            <?php
        }

            ?>
            </tbody>
    </table>
<?php
} else {
?>


    <center>
        <h1 class="text-danger">Erreur lors de la connexion administrateur</h1>
        <a href="/exosPHP/bonbonsMVC/index.php" class="btn btn-danger">Retourner à l'accueil</a>
    </center>

<?php
}
$content = ob_get_clean();
require("template.php");
