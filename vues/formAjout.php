<?php
ob_start();
if (isset($_SESSION["autorisation"]) and $_SESSION["autorisation"] == "ok") {
?>
    <div class="container">

        <form action="index.php?uc=admin&choix=confAjout" method="post" enctype="multipart/form-data">
            <div class="container">
                <div class="row">
                    <div class="form-group">
                        <label for="nom">Nom du produit</label>
                        <input type="text" class="form-control" id="nom" name="nom">
                    </div>
                    <div class="form-group">
                        <label for="nom">prix du produit</label>
                        <input type="text" class="form-control" id="nom" name="prix">
                    </div>
                    <div class="form-group">
                        <label for="type">L'id de la categorie (1 pour bonbon et 2 pour biscuit)</label>
                        <input type="number" class="form-control" name="idCateg" id="type">
                    </div>
                    <br><br><br>
                    <div class="form-group">
                        <label for="fichier">Fichier</label>
                        <input type="file" class="form-control-file" id="fichier" name="img">
                    </div>
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </div>
        </form>
    </div>

<?php
} else {
?>

    <center>
        <h1 class="text-danger">Erreur lors de la connexion administrateur</h1>
        <a href="/exosPHP/bonbonsMVC/index.php" class="btn btn-danger">Retourner à l'accueil</a>
    </center>

<?php
}
$content = ob_get_clean();
require("template.php"); ?>