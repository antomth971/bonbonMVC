<?php
ob_start();
if (!empty($_SESSION['succes'])) {
?>
    <div class="alert alert-succes" role="alert" id="alert">
        <?php echo $_SESSION['succes'] ?>
    </div>
<?php
    unset($_SESSION['succes']);
}
if (!empty($_SESSION['panier'])) {

?>
    <div class="container">
        <div class="row">
            <h1>PANIER</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th>produit</th>
                        <th>Prix unité</th>
                        <th>Quantité</th>
                        <th>Montant</th>
                        <th>Ajouter un paquet</th>
                        <th>Retirer un paquet</th>
                    </tr>
                </thead>
                <?php
                $total = 0;
                foreach ($_SESSION['panier'] as $id => $quantite) {
                    $produit = Produit::panier($quantite, $id);
                    $montant = $produit->montant;
                ?>

                    <tbody>
                        <tr>
                            <td><?php echo $produit->getNom() ?></td>
                            <td><?php echo $produit->getPrix() . "€" ?></td>
                            <td><?php echo $quantite ?></td>
                            <td><?php echo $montant . "€" ?></td>
                            <td><a href="index.php?uc=bonbons&action=ajout&ajoutPanier=<?php echo $produit->getId() ?>"><img src="Images/ajouterPanier.jpg" alt="ajouter le meme article"></a></td>
                            <td><a href="index.php?uc=bonbons&action=retrait&retraitPanier=<?php echo $produit->getId() ?>"><img src="Images/retirerPanier.jpg" alt="retirer du panier"></a></td>

                        </tr>



                    <?php
                    // $prixHT = (array_sum($_SESSION['panier'])) * $produit->prix;


                    $total += $montant;
                    $prixHT = round($total, 2);
                }
                    ?>



                    </tbody>
            </table>
        </div>
        <div class="row">
            <a href="index.php?uc=bonbons&action=viderPanier"><input class="btn btn-primary" type="button" value="vider le panier"></a>
        </div>
    </div>
    <div class="row">
        <div class="col-6"></div>
        <div class="col-6" style="background-color: black;">
            <table class="table" style="color:white;">
                <tr>
                    <td>Total HT</td>
                    <td><?php echo $prixHT . "€" ?></td>
                </tr>
                <tr>
                    <td>TVA(20%)</td>
                    <td><?php echo $tva = $prixHT * 0.20  ?></td>
                </tr>
                <tr>
                    <td>Frais de port</td>
                    <td>5€</td>
                </tr>
                <tr>
                    <td>Total TTC</td>
                    <td><?php echo $prixHT + $tva . "€" ?></td>

                </tr>
            </table>
        </div>
        <div class="col-6"></div>
        <div class="col-6">
            <div class="row">
                <center>
                    <a href="index.php?uc=bonbons&action=liste"><input class="btn btn-primary" type="button" value="Continuer mes achats"></a>
                </center>
            </div>
            <div class="row">
                <center>
                    <a href=""><input class="btn btn-primary" type="button" value="Payer"></a>
                </center>
            </div>
        </div>
    </div>















<?php
} else {

?>
    <center>
        <h1 class="text-danger">Votre panier est vide</h1>
    </center>
    <a href="index.php?uc=bonbons&action=liste" class="btn btn-dark">retournez sur la page d'accueil</a>
<?php
}
$content = ob_get_clean();
require("template.php"); ?>