<?php
ob_start();
?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <H2>Nos Produits par Catégorie</H2>
        </div>
    </div>
    <div class="col">
        <h3 class="text-danger"><?= $laCategorie->getLibelle(); ?></h3>
        <br>

        <?php
        foreach ($produitsCategories as $produit) {
        ?>

            <div class="card text-center" style="width: 15rem;">
                <img src="Images/<?= $produit->getPhoto() ?>" class="card-img-top">
                <h5 class='card-title'> <?= $produit->getNom() ?></h5>
                <p class='card-text'> <?= $produit->getPrix() ?> €</p>
            </div>



        <?php
        }


        ?>
    </div>
</div>
<?php $content = ob_get_clean();
require("template.php"); ?>