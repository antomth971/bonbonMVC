<?php ob_start(); ?>

<form action="index.php?uc=client&user=verifNouveau" method="POST">
    <div class="form-group">
        <label for="prenom1">Prénom</label>
        <input required type="text" name="prenomNouveau" class="form-control" id="prenom1" placeholder="prénom">
    </div>
    <div class="form-group">
        <label for="nom1">Nom</label>
        <input required type="text" name="nomNouveau" class="form-control" id="nom1" placeholder="nom">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input required type="text" name="mailNouveau" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Mot de passe</label>
        <input required type="password" name="mdpNouveau" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>

    <button type="submit" class="btn btn-primary">Créer</button>
</form>
<center>
    <a href="user.php" class="btn btn-secondary">Vous avez déjà un compte, veuillez aller vous connecter</a>
</center>
<?php $content = ob_get_clean();
require("template.php"); ?>