<?php ob_start(); ?>

<form method="POST" action="index.php?uc=admin&choix=verif">
    <div class="form-group">
        <label for="exampleInputEmail1">Identifiant</label>
        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="login">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Mot de passe</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="mdp">
    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
    </div>
    <button type="submit" class="btn btn-primary">Connectez-vous</button>
</form>
<?php $content = ob_get_clean();
require("template.php"); ?>