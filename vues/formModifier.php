<?php
ob_start();
if (isset($_SESSION["autorisation"]) and $_SESSION["autorisation"] == "ok") {
    var_dump($produit);

?>

    <form action="index.php?uc=admin&choix=confModifier" method="post" enctype="multipart/form-data">
        <div class="container">
            <div class="row">
                <div class="form-group">
                    <label for="nom">Nom du produit</label>
                    <input type="text" class="form-control" id="nom" name="nom" value="<?= $produit->getNom() ?>">
                </div>
                <div class="form-group">
                    <label for="nom">Prix du produit</label>
                    <input type="text" class="form-control" id="nom" value="<?= $produit->getPrix() ?>" name="prix">
                </div>
                <div class="form-group">
                    <label for="nom">Id de la categorie du produit</label>
                    <input type="number" class="form-control" id="nom" value="<?= $produit->getIdCat2() ?>" name="idCat">
                </div>
                <div class="form-group">
                    <label for="fichier"><?= $produit->getPhoto() ?></label>
                    <input type="file" class="form-control-file" id="fichier" name="img">
                    <input type="hidden" name="idCache" value="<?php echo $produit->getId() ?>">
                </div>
                <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>

        </div>
    </form>
    </div>

    <?php
    $content = ob_get_clean();
    require("template.php"); ?>















    ?>
<?php
} else {
    echo "vous'navez pas le droit d'être sur cette page !!!";
    echo "retournez sur <a href='/exosPHP/bonbonsMVC/index.php'>accueil</a>  ";
}
